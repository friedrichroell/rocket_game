function GameObject(x, y)
{
    this.type = ""
    this.x = 0 || x
    this.y = 0 || y
    this.width = 0
    this.height = 0
    this.rotation = 0
    this.image = ""
    this.color = "black"
    this.speed = new Victor(0,0)
    this.dead = false

    this.cycleLogic = function() {
        this.x -= this.speed.x
        this.y -= this.speed.y
    }
}

GameObject.prototype.draw = function()
{
    if (this.dead) {
        return
    }

    if (this.image) {
        this.drawImage()
        return
    }

    this.drawRect()
}

GameObject.prototype.drawRect = function()
{
    ENV.ctx.fillStyle = this.color
    ENV.ctx.save()
    ENV.ctx.translate(this.x, this.y);
    ENV.ctx.rotate(this.rotation * Math.PI/180);
    ENV.ctx.fillRect(-this.width / 2, -this.height / 2, this.width, this.height)
    ENV.ctx.restore()
}

GameObject.prototype.drawImage = function()
{
    let img = new Image();
    img.src = this.image;
    ENV.ctx.save()
    ENV.ctx.translate(this.x, this.y);
    ENV.ctx.rotate(this.rotation * Math.PI/180);
    ENV.ctx.drawImage(img, -this.width / 2, -this.height / 2, this.width, this.height);
    ENV.ctx.restore()
}

GameObject.prototype.getPosition = function()
{
    return new Position(this.x, this.y)
}

GameObject.prototype.hasCollision = function(game_object)
{
    return CollisionHandler.hasCollision(this, game_object)
}

GameObject.prototype.isOutOfBounds = function (overflow) {
    overflow = overflow || 0

    return this.x > ENV.canvas.width + overflow
        || this.x < -overflow
        || this.y > ENV.canvas.height + overflow
        || this.y < -overflow
}

GameObject.prototype.move = function()
{
    this.x -= this.speed.x * ENV.delta_time
    this.y -= this.speed.y * ENV.delta_time
}

GameObject.prototype.kill = function()
{
    this.dead = true
}
