function Game()
{
    this.game_interval = null
    this.last_draw = new Date()
    this.last_robot_action = new Date()
    this.beast_mode = false

    this.loadGameObjects()
}

Game.prototype.run = function(speed)
{
    if (null === this.game_interval) {
        this.game_interval = setInterval(() => this.doCycle(), speed)
    }
}

Game.prototype.isRunning = function()
{
    return null !== this.game_interval
}

Game.prototype.stop = function()
{
    clearInterval(this.game_interval)
    this.game_interval = null
}

Game.prototype.loadGameObjects = function()
{
    let rocket = GameObjectDefinitions.getRocket()

    ENV.rocket = rocket
    ENV.addGameObject(rocket)
}

Game.prototype.doCycle = function()
{
    ENV.delta_time = (new Date - ENV.last_loop) / 1000
    ENV.last_loop = new Date

    // fps
    let shouldDraw = new Date - this.last_draw >= 1000/60

    if (shouldDraw) {
        ENV.ctx.clearRect(0, 0, ENV.canvas.width, ENV.canvas.height)
        this.last_draw = new Date
        this.drawLayout()
    }

    if (this.beast_mode) {
        this.robot()
    }

    for (let object of ENV.game_objects) {
        object.cycleLogic()

        if (shouldDraw) {
            object.draw()
        }
    }

    for (let object of ENV.paint_objects) {
        if (shouldDraw) {
            object.draw()
        }
    }

    this.cleanUpDeadObjects()
    this.makeEnemies()
}

Game.prototype.cleanUpDeadObjects = function()
{
    for (let index = 0; index < ENV.game_objects.length; index++) {
        let object = ENV.game_objects[index]

        if (object.dead) {
            ENV.game_objects.splice(index, 1)
            index--
        }
    }

    for (let index = 0; index < ENV.paint_objects.length; index++) {
        let object = ENV.paint_objects[index]

        if (object.dead) {
            ENV.paint_objects.splice(index, 1)
            index--
        }
    }
}

Game.prototype.makeEnemies = function()
{
    let living_enemies = 0

    for (let enemy of ENV.game_objects) {
        if (!enemy.dead && enemy.type === "enemy") {
            living_enemies++
        }
    }

    let count = 7 - living_enemies

    for (let i = 0; i < count; i++) {
        let rock = GameObjectDefinitions.makeRock()
        rock.x = ENV.canvas.width * Math.random()
        rock.y = ENV.canvas.height * Math.random()

        let rotation = 365 * Math.random()
        rock.speed = new Victor(1,1)
        rock.speed.multiplyScalar(Math.random() * 60 + 30)
        rock.speed.rotateToDeg(rotation)
        rock.rotation = rotation

        ENV.game_objects.push(rock)
    }
}

Game.prototype.drawLayout = function()
{
    ENV.ctx.beginPath();
    ENV.ctx.arc(ENV.mouse_position.x, ENV.mouse_position.y, 15, 0 , 2*Math.PI);
    ENV.ctx.strokeStyle = "white"
    ENV.ctx.stroke();

    ENV.ctx.font = "20px Monospace";
    ENV.ctx.fillStyle = "#fff7bf"
    ENV.ctx.fillText("hits: " + ENV.enemy_hits + " misses: " + ENV.enemy_misses + " hit ratio: " + parseFloat(ENV.enemy_hits / ENV.enemy_misses).toFixed(2) , ENV.canvas.width / 2 - 70 ,20);
}

Game.prototype.robot = function ()
{
    if (new Date - this.last_robot_action <= 700) {
        return
    }

    this.last_robot_action = new Date

    for (let enemy of ENV.game_objects) {
        if (!enemy.dead && enemy.type === "enemy" && !enemy.isOutOfBounds() && !enemy.hasBeenTargeted) {

            let enemy_vector = ENV.rocket.getPosition().GetVectorToPosition(enemy.getPosition())

            let kill_time = ENV.calcPredictionShot(enemy)

            enemy_vector.add(enemy.speed.clone().multiplyScalar(kill_time))
            enemy.color = "#ff2d5e"
            enemy.hasBeenTargeted = true

            enemy.aim_dot.x = ENV.rocket.x - enemy_vector.x
            enemy.aim_dot.y = ENV.rocket.y - enemy_vector.y
            enemy.aim_dot.kill_time = Date.now() + kill_time * 1000

            ENV.rocket.rotation = enemy_vector.angleDeg() - 134
            ENV.rocket.shoot()

           break
        }
    }
}
















