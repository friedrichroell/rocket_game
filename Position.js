function Position(x ,y) {
    this.x = 0 || x
    this.y = 0 || y
}

/**
 * @param {Position} position
 * @returns {Victor}
 */
Position.prototype.GetVectorToPosition = function (position) {
    return new Victor(this.x - position.x, this.y - position.y)
}
