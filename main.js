ENV = new Environment()

window.onload = () => {
    document.body.appendChild(ENV.canvas)

    let game = new Game()

    game.run(1)

    let key_press_handler = (e) => {
        if (e.keyCode === 98) {
            game.beast_mode = !game.beast_mode
        }
        if (112 !== e.keyCode) {
            return
        }

        if (game.isRunning()) {
            game.stop()
            return
        }

        game.run(1)
    }

    window.addEventListener('keypress', key_press_handler, true);

}


