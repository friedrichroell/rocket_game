let CollisionHandler = (function()
{
    /**
     *
     * @param {GameObject} object1
     * @param {GameObject} object2
     *
     * @return boolean
     */
    let hasCollision = function(object1, object2)
    {
        if ( ! hasLargeCollision(object1, object2)) {
            return false
        }

        let rect1_corners = findCornersFromRectangle(object1.x, object1.y, object1.width, object1.height, object1.rotation)
        let rect2_corners = findCornersFromRectangle(object2.x, object2.y, object2.width, object2.height, object2.rotation)

        let rect1_width_axis  = new Victor(rect1_corners[1].x - rect1_corners[0].x, rect1_corners[1].y - rect1_corners[0].y)
        let rect1_height_axis = new Victor(rect1_corners[3].x - rect1_corners[0].x, rect1_corners[3].y - rect1_corners[0].y)

        // rect1_width_axis

        let rect1_projection_width_axis_rect1_p1 = projectToAxis(rect1_width_axis, rect1_corners[0])
        let rect1_projection_width_axis_rect1_p2 = projectToAxis(rect1_width_axis, rect1_corners[1])

        let rect1_projection_width_axis_rect2_p1 = projectToAxis(rect1_width_axis, rect2_corners[0])
        let rect1_projection_width_axis_rect2_p2 = projectToAxis(rect1_width_axis, rect2_corners[1])
        let rect1_projection_width_axis_rect2_p3 = projectToAxis(rect1_width_axis, rect2_corners[2])
        let rect1_projection_width_axis_rect2_p4 = projectToAxis(rect1_width_axis, rect2_corners[3])

        let rect1_projection_width_axis_rect1_min_x = Math.min(rect1_projection_width_axis_rect1_p1.x, rect1_projection_width_axis_rect1_p2.x)
        let rect1_projection_width_axis_rect1_min_y = Math.min(rect1_projection_width_axis_rect1_p1.y, rect1_projection_width_axis_rect1_p2.y)
        let rect1_projection_width_axis_rect1_max_x = Math.max(rect1_projection_width_axis_rect1_p1.x, rect1_projection_width_axis_rect1_p2.x)
        let rect1_projection_width_axis_rect1_max_y = Math.max(rect1_projection_width_axis_rect1_p1.y, rect1_projection_width_axis_rect1_p2.y)

        let rect1_projection_width_axis_rect2_min_x = Math.min(rect1_projection_width_axis_rect2_p1.x, rect1_projection_width_axis_rect2_p2.x, rect1_projection_width_axis_rect2_p3.x, rect1_projection_width_axis_rect2_p4.x)
        let rect1_projection_width_axis_rect2_min_y = Math.min(rect1_projection_width_axis_rect2_p1.y, rect1_projection_width_axis_rect2_p2.y, rect1_projection_width_axis_rect2_p3.y, rect1_projection_width_axis_rect2_p4.y)
        let rect1_projection_width_axis_rect2_max_x = Math.max(rect1_projection_width_axis_rect2_p1.x, rect1_projection_width_axis_rect2_p2.x, rect1_projection_width_axis_rect2_p3.x, rect1_projection_width_axis_rect2_p4.x)
        let rect1_projection_width_axis_rect2_max_y = Math.max(rect1_projection_width_axis_rect2_p1.y, rect1_projection_width_axis_rect2_p2.y, rect1_projection_width_axis_rect2_p3.y, rect1_projection_width_axis_rect2_p4.y)

        let no_collision =
               rect1_projection_width_axis_rect1_max_x < rect1_projection_width_axis_rect2_min_x || rect1_projection_width_axis_rect1_min_x > rect1_projection_width_axis_rect2_max_x
            || rect1_projection_width_axis_rect1_max_y < rect1_projection_width_axis_rect2_min_y || rect1_projection_width_axis_rect1_min_y > rect1_projection_width_axis_rect2_max_y

        if (no_collision) {
            return false
        }

        // rect1_height_axis

        let rect1_projection_height_axis_rect1_p1 = projectToAxis(rect1_height_axis, rect1_corners[0])
        let rect1_projection_height_axis_rect1_p2 = projectToAxis(rect1_height_axis, rect1_corners[3])

        let rect1_projection_height_axis_rect2_p1 = projectToAxis(rect1_height_axis, rect2_corners[0])
        let rect1_projection_height_axis_rect2_p2 = projectToAxis(rect1_height_axis, rect2_corners[1])
        let rect1_projection_height_axis_rect2_p3 = projectToAxis(rect1_height_axis, rect2_corners[2])
        let rect1_projection_height_axis_rect2_p4 = projectToAxis(rect1_height_axis, rect2_corners[3])

        let rect1_projection_height_axis_rect1_min_x = Math.min(rect1_projection_height_axis_rect1_p1.x, rect1_projection_height_axis_rect1_p2.x)
        let rect1_projection_height_axis_rect1_min_y = Math.min(rect1_projection_height_axis_rect1_p1.y, rect1_projection_height_axis_rect1_p2.y)
        let rect1_projection_height_axis_rect1_max_x = Math.max(rect1_projection_height_axis_rect1_p1.x, rect1_projection_height_axis_rect1_p2.x)
        let rect1_projection_height_axis_rect1_max_y = Math.max(rect1_projection_height_axis_rect1_p1.y, rect1_projection_height_axis_rect1_p2.y)

        let rect1_projection_height_axis_rect2_min_x = Math.min(rect1_projection_height_axis_rect2_p1.x, rect1_projection_height_axis_rect2_p2.x, rect1_projection_height_axis_rect2_p3.x, rect1_projection_height_axis_rect2_p4.x)
        let rect1_projection_height_axis_rect2_min_y = Math.min(rect1_projection_height_axis_rect2_p1.y, rect1_projection_height_axis_rect2_p2.y, rect1_projection_height_axis_rect2_p3.y, rect1_projection_height_axis_rect2_p4.y)
        let rect1_projection_height_axis_rect2_max_x = Math.max(rect1_projection_height_axis_rect2_p1.x, rect1_projection_height_axis_rect2_p2.x, rect1_projection_height_axis_rect2_p3.x, rect1_projection_height_axis_rect2_p4.x)
        let rect1_projection_height_axis_rect2_max_y = Math.max(rect1_projection_height_axis_rect2_p1.y, rect1_projection_height_axis_rect2_p2.y, rect1_projection_height_axis_rect2_p3.y, rect1_projection_height_axis_rect2_p4.y)

        no_collision =
               rect1_projection_height_axis_rect1_max_x < rect1_projection_height_axis_rect2_min_x ||  rect1_projection_height_axis_rect1_min_x > rect1_projection_height_axis_rect2_max_x
            || rect1_projection_height_axis_rect1_max_y < rect1_projection_height_axis_rect2_min_y || rect1_projection_height_axis_rect1_min_y > rect1_projection_height_axis_rect2_max_y

        if (no_collision) {
            return false
        }

        // rect2_width_axis

        let rect2_width_axis = new Victor(rect2_corners[1].x - rect2_corners[0].x, rect2_corners[1].y - rect2_corners[0].y).normalize()
        let rect2_height_axis = new Victor(rect2_corners[3].x - rect2_corners[0].x, rect2_corners[3].y - rect2_corners[0].y).normalize()

        let rect2_projection_width_axis_rect2_p1 = projectToAxis(rect2_width_axis, rect2_corners[0])
        let rect2_projection_width_axis_rect2_p2 = projectToAxis(rect2_width_axis, rect2_corners[1])

        let rect2_projection_width_axis_rect1_p1 = projectToAxis(rect2_width_axis, rect1_corners[0])
        let rect2_projection_width_axis_rect1_p2 = projectToAxis(rect2_width_axis, rect1_corners[1])
        let rect2_projection_width_axis_rect1_p3 = projectToAxis(rect2_width_axis, rect1_corners[2])
        let rect2_projection_width_axis_rect1_p4 = projectToAxis(rect2_width_axis, rect1_corners[3])

        let rect2_projection_width_axis_rect2_min_x = Math.min(rect2_projection_width_axis_rect2_p1.x, rect2_projection_width_axis_rect2_p2.x)
        let rect2_projection_width_axis_rect2_min_y = Math.min(rect2_projection_width_axis_rect2_p1.y, rect2_projection_width_axis_rect2_p2.y)
        let rect2_projection_width_axis_rect2_max_x = Math.max(rect2_projection_width_axis_rect2_p1.x, rect2_projection_width_axis_rect2_p2.x)
        let rect2_projection_width_axis_rect2_max_y = Math.max(rect2_projection_width_axis_rect2_p1.y, rect2_projection_width_axis_rect2_p2.y)

        let rect2_projection_width_axis_rect1_min_x = Math.min(rect2_projection_width_axis_rect1_p1.x, rect2_projection_width_axis_rect1_p2.x, rect2_projection_width_axis_rect1_p3.x, rect2_projection_width_axis_rect1_p4.x)
        let rect2_projection_width_axis_rect1_min_y = Math.min(rect2_projection_width_axis_rect1_p1.y, rect2_projection_width_axis_rect1_p2.y, rect2_projection_width_axis_rect1_p3.y, rect2_projection_width_axis_rect1_p4.y)
        let rect2_projection_width_axis_rect1_max_x = Math.max(rect2_projection_width_axis_rect1_p1.x, rect2_projection_width_axis_rect1_p2.x, rect2_projection_width_axis_rect1_p3.x, rect2_projection_width_axis_rect1_p4.x)
        let rect2_projection_width_axis_rect1_max_y = Math.max(rect2_projection_width_axis_rect1_p1.y, rect2_projection_width_axis_rect1_p2.y, rect2_projection_width_axis_rect1_p3.y, rect2_projection_width_axis_rect1_p4.y)

        no_collision =
               rect2_projection_width_axis_rect2_max_x < rect2_projection_width_axis_rect1_min_x || rect2_projection_width_axis_rect2_min_x > rect2_projection_width_axis_rect1_max_x
            || rect2_projection_width_axis_rect2_max_y < rect2_projection_width_axis_rect1_min_y || rect2_projection_width_axis_rect2_min_y > rect2_projection_width_axis_rect1_max_y

        if (no_collision) {
            return false
        }

        // rect2_height_axis

        let rect2_projection_height_axis_rect2_p1 = projectToAxis(rect2_height_axis, rect2_corners[0])
        let rect2_projection_height_axis_rect2_p2 = projectToAxis(rect2_height_axis, rect2_corners[3])

        let rect2_projection_height_axis_rect1_p1 = projectToAxis(rect2_height_axis, rect1_corners[0])
        let rect2_projection_height_axis_rect1_p2 = projectToAxis(rect2_height_axis, rect1_corners[1])
        let rect2_projection_height_axis_rect1_p3 = projectToAxis(rect2_height_axis, rect1_corners[2])
        let rect2_projection_height_axis_rect1_p4 = projectToAxis(rect2_height_axis, rect1_corners[3])

        let rect2_projection_height_axis_rect2_min_x = Math.min(rect2_projection_height_axis_rect2_p1.x, rect2_projection_height_axis_rect2_p2.x)
        let rect2_projection_height_axis_rect2_min_y = Math.min(rect2_projection_height_axis_rect2_p1.y, rect2_projection_height_axis_rect2_p2.y)
        let rect2_projection_height_axis_rect2_max_x = Math.max(rect2_projection_height_axis_rect2_p1.x, rect2_projection_height_axis_rect2_p2.x)
        let rect2_projection_height_axis_rect2_max_y = Math.max(rect2_projection_height_axis_rect2_p1.y, rect2_projection_height_axis_rect2_p2.y)

        let rect2_projection_height_axis_rect1_min_x = Math.min(rect2_projection_height_axis_rect1_p1.x, rect2_projection_height_axis_rect1_p2.x, rect2_projection_height_axis_rect1_p3.x, rect2_projection_height_axis_rect1_p4.x)
        let rect2_projection_height_axis_rect1_min_y = Math.min(rect2_projection_height_axis_rect1_p1.y, rect2_projection_height_axis_rect1_p2.y, rect2_projection_height_axis_rect1_p3.y, rect2_projection_height_axis_rect1_p4.y)
        let rect2_projection_height_axis_rect1_max_x = Math.max(rect2_projection_height_axis_rect1_p1.x, rect2_projection_height_axis_rect1_p2.x, rect2_projection_height_axis_rect1_p3.x, rect2_projection_height_axis_rect1_p4.x)
        let rect2_projection_height_axis_rect1_max_y = Math.max(rect2_projection_height_axis_rect1_p1.y, rect2_projection_height_axis_rect1_p2.y, rect2_projection_height_axis_rect1_p3.y, rect2_projection_height_axis_rect1_p4.y)

        no_collision =
               rect2_projection_height_axis_rect2_max_x < rect2_projection_height_axis_rect1_min_x || rect2_projection_height_axis_rect2_min_x > rect2_projection_height_axis_rect1_max_x
            || rect2_projection_height_axis_rect2_max_y < rect2_projection_height_axis_rect1_min_y || rect2_projection_height_axis_rect2_min_y > rect2_projection_height_axis_rect1_max_y

        return ! no_collision
    }

    /**
     *
     * @param {GameObject} object1
     * @param {GameObject} object2
     *
     * @return boolean
     */
    let hasLargeCollision =  function(object1, object2)
    {
        let safe_obj1 = createSafeObject(object1)
        let safe_obj2 = createSafeObject(object2)

        let x_min_ob1 = safe_obj1.x - safe_obj1.width / 2
        let x_max_ob1 = safe_obj1.x + safe_obj1.width / 2

        let x_min_ob2 = safe_obj2.x - safe_obj2.width / 2
        let x_max_ob2 = safe_obj2.x + safe_obj2.width / 2

        if (x_max_ob1 < x_min_ob2 || x_min_ob1 > x_max_ob2) {
            return false
        }

        let y_min_ob1 = safe_obj1.y - safe_obj1.height / 2
        let y_max_ob1 = safe_obj1.y + safe_obj1.height / 2

        let y_min_ob2 = safe_obj2.y - safe_obj2.height / 2
        let y_max_ob2 = safe_obj2.y + safe_obj2.height / 2

        return ! (y_max_ob1 < y_min_ob2 || y_min_ob1 > y_max_ob2)
    }

    /**
     * @param {GameObject} object
     * @return {GameObject}
     */
    let createSafeObject = function (object)
    {
        let edge = 2 * Math.max(object.width, object.height)

        let safe_object =  new GameObject(object.x, object.y)

        safe_object.width  = edge
        safe_object.height = edge

        return safe_object
    }


    /**
     * returned in clockwise order starting from the upper left corner
     *
     * @param x
     * @param y
     * @param width
     * @param height
     * @param rotation
     */
    let findCornersFromRectangle = function(x, y, width, height, rotation)
    {
         let urbl = rotatePoint(0,0, width / 2, height / 2, rotation)
         let ulbr = rotatePoint(0,0, width / 2, -height / 2, rotation)

        return [
            new Position(x - ulbr[0], y + ulbr[1]),
            new Position(x + urbl[0], y - urbl[1]),
            new Position(x + ulbr[0], y - ulbr[1]),
            new Position(x - urbl[0], y + urbl[1]),
        ]
    }

    let rotatePoint = function(cx, cy, x, y, angle)
    {
        let radians = (Math.PI / 180) * angle,
            cos = Math.cos(radians),
            sin = Math.sin(radians),
            nx = (cos * (x - cx)) + (sin * (y - cy)) + cx,
            ny = (cos * (y - cy)) - (sin * (x - cx)) + cy;

        return [nx, ny]
    }

    /**
     *
     * @param {Victor} axis
     * @param {Position} point
     * @returns {Position}
     */
    let projectToAxis = function(axis, point)
    {
        let pre_calc = (axis.x * point.x + axis.y * point.y) /
            (Math.pow(axis.x, 2) + Math.pow(axis.y, 2))

        return new Position(pre_calc * axis.x, pre_calc * axis.y)
    }

    return {
        hasCollision : hasCollision
    }
})()
