function Environment()
{
    this.canvas = null
    this.ctx = null
    this.mouse_position = new Position()

    this.last_loop = new Date
    this.delta_time = 1

    this.rocket = null
    this.game_objects = []
    this.paint_objects = []

    this.pressed_keys = []
    this.has_key = {}

    this.enemy_hits = 0
    this.enemy_misses = 0

    this.window_width = isNaN(window.innerWidth) ? window.clientWidth : window.innerWidth;
    this.window_height = isNaN(window.innerHeight) ? window.clientHeight : window.innerHeight;

    this.loadCanvas()
    this.loadMousePosition()
    this.loadEventHandler()
    this.loadHasKeyFunctions()
}

Environment.prototype.loadCanvas = function()
{
    this.canvas = document.createElement("canvas")

    this.canvas.style.width = this.window_width + "px"
    this.canvas.style.height = this.window_height + "px"
    this.canvas.style.background = "#498ac6"
    this.canvas.width = this.window_width;
    this.canvas.height = this.window_height;

    this.ctx = this.canvas.getContext("2d")
}

Environment.prototype.loadMousePosition = function()
{
    this.mouse_position = new Position(parseInt(this.window_width / 2), parseInt(this.window_height / 2))
    let self = this

    window.onmousemove = (e) => {

        let rect = self.canvas.getBoundingClientRect();
        self.mouse_position.x = e.clientX - rect.left
        self.mouse_position.y = e.clientY - rect.top
    }
}

Environment.prototype.loadEventHandler = function()
{
    let key_down_handler = (e) => {
        let index = this.pressed_keys.indexOf(e.keyCode)
        if (index === -1) {
            this.pressed_keys.push(e.keyCode)
        }
    }

    let key_up_handler = (e) => {
        let index = this.pressed_keys.indexOf(e.keyCode)
        if (index > -1) {
            this.pressed_keys.splice(index, 1)
        }
    }

    window.addEventListener('keydown', key_down_handler, true);
    window.addEventListener('keyup', key_up_handler, true);
}

Environment.prototype.loadHasKeyFunctions = function ()
{
    let hasKey = keyCode => this.pressed_keys.indexOf(keyCode) > -1

    this.has_key = {
        space : () => hasKey(32),
        w     : () => hasKey(87),
    }
}

Environment.prototype.addGameObject = function(object)
{
    this.game_objects.push(object)
}

Environment.prototype.calcPredictionShot = function(enemy)
{
    let sq = (x) => Math.pow(x, 2)

    let AB = ENV.rocket.getPosition().GetVectorToPosition(enemy.getPosition())
    let vx = enemy.speed.x
    let vy = enemy.speed.y
    let w  = 2000
    let ax = AB.x
    let ay = AB.y

    let a = (0.5 * (-2 * ax * vx - 2 * ay * vy  -
        2 * Math.sqrt(-1 * sq(ay) * sq(vx) + 2 * ax * ay * vx * vy - sq(ax) * sq(vy) + sq(ax) * sq(w) +
            sq(ay) * sq(w)))) / (sq(vx) + sq(vy) - sq(w))

    let b = (0.5 * (-2 * ax * vx - 2 * ay * vy +
        2 * Math.sqrt(-1 * sq(ay) * sq(vx) + 2 * ax * ay * vx * vy -1 * sq(ax) * sq(vy) + sq(ax) * sq(w) +
            sq(ay) * sq(w)))) / (sq(vx) + sq(vy) - sq(w))

    return Math.max(a, b)
}
