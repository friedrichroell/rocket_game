function GameObjectDefinitions() {}

GameObjectDefinitions.getRocket = function()
{
    let rocket = new GameObject(500, 500)

    rocket.image = "assets/rocket.svg"
    rocket.width = 50
    rocket.height = 50

    rocket.last_shot = 0

    rocket.shoot = () => {
        ENV.game_objects.push(GameObjectDefinitions.makeProjectile(rocket))
    }

    rocket.cycleLogic = () => {

        let mouse_vector = rocket.getPosition().GetVectorToPosition(ENV.mouse_position)
        rocket.rotation = mouse_vector.angleDeg() - 134

        if (ENV.has_key.w()) {
            let acceleration = mouse_vector.normalize().multiplyScalar(2000 * ENV.delta_time)
            rocket.speed.add(acceleration)
        }

        let friction = rocket.speed.clone()
        friction.rotateDeg(180).multiplyScalar(1.5 * ENV.delta_time)

        rocket.speed.add(friction)

        rocket.move()

        if (ENV.has_key.space() && 200 <= (new Date() - rocket.last_shot)) {
            rocket.last_shot = new Date
            rocket.shoot()
        }
    }

    return rocket
}

GameObjectDefinitions.makeRock = function()
{
    let rock = new GameObject()

    rock.type = "enemy"
    rock.width = 25
    rock.height= 5

    rock.color = "#4ec4bd"

    let aim_dot = new GameObject()
    aim_dot.width = 3
    aim_dot.height= 3
    aim_dot.color = "white"
    aim_dot.kill_time = 0

    ENV.paint_objects.push(aim_dot)

    rock.aim_dot = aim_dot

    rock.cycleLogic = () => {
        if (rock.isOutOfBounds(250)) {
            rock.kill()

            return
        }

        if (rock.aim_dot.x && Date.now() - aim_dot.kill_time >= 0) {
            rock.color = "#c4269d"
            rock.hasBeenTargeted = false
        }

        rock.move()
    }

    rock.kill = function() {
        rock.dead = true
        rock.aim_dot.dead = true
    }

    return rock
}

GameObjectDefinitions.makeProjectile = function(shooter)
{
    let projectile = new GameObject(shooter.x, shooter.y)
    let snd = new Audio("assets/kick.wav")

    projectile.width  = 50
    projectile.height = 3
    projectile.color = "#fff7bf"
    projectile.rotation = shooter.rotation + 134

    projectile.start_time = new Date

    projectile.speed = new Victor(1,1)
    projectile.speed.rotateToDeg(shooter.rotation + 134).normalize().multiplyScalar(2000)
    //projectile.speed.add(shooter.speed)


    snd.volume = 0.2
    snd.play();

    projectile.cycleLogic = () => {
        if (projectile.isOutOfBounds(800)) {
            ENV.enemy_misses++
            projectile.kill()
            return
        }

        if (has_collision()) {
            return
        }

        projectile.move()
    }

    let has_collision = () => {
        for (let enemy of ENV.game_objects) {
            if ("enemy" !== enemy.type || enemy.dead) {
                continue
            }

            if (projectile.hasCollision(enemy)) {
                projectile.kill()
                enemy.kill()
                GameObjectDefinitions.makeExplosion(enemy)
                ENV.enemy_hits++

                return true
            }
        }

        return false
    }

    return projectile
}

GameObjectDefinitions.makeExplosion = function(target)
{
    let snd = new Audio("assets/crate_break.wav")
    snd.volume = 0.2
    snd.play();

    for (let x = 0; x < 40; x++) {
        let explosion = new GameObject()

        explosion.type = "explosion"
        explosion.spawn_time = new Date
        explosion.opacity = 1
        explosion.color_object = { r : 238, g: 238, b: 88}

        explosion.width = 2
        explosion.height = 2

        explosion.x = target.x
        explosion.y = target.y

        let sign_x = Math.random() >= 0.5 ? -1 : 1
        let sign_y = Math.random() >= 0.5 ? -1 : 1

        explosion.speed = new Victor(1,1)
        explosion.speed.multiplyScalar(Math.random() * 200)
        explosion.speed.rotateToDeg(365 * Math.random())


        explosion.cycleLogic = () => {

            let time_diff = new Date() - explosion.spawn_time
            let life_time = 2000

            if (life_time <= time_diff) {
                explosion.kill()
                return
            }

            let r = explosion.color_object.r
            let g = explosion.color_object.g
            let b = explosion.color_object.b
            let a = 1 - time_diff / life_time

            explosion.color = "rgba("+r+", "+g+", "+b+", "+a+")"

            explosion.move()
        }

        ENV.game_objects.push(explosion)
    }
}

